const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const races = require('./race.json');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, 'source')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/login', function (req, res) {
  res.sendFile(path.join(__dirname, 'login.html'));
});

app.get('/game', function (req, res) {
  res.sendFile(path.join(__dirname, 'game.html'));
});

app.post('/login', function (req, res) {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, 'someSecret', { expiresIn: '24h' });
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

let timer;
const gameReady = {
  race: '',
  usersList: []
};
const comment = {
  accost: 'Отож ми розпочинаємо нашу гонку і для вас сьогодні коментує Джон, гарно сідаємо перед екранами і поїхали...',
  listMember: [],
  listFinished: [],
  finishedCount: 0
}

function playGame() {
  comment.finishedCount = 0;
  for (let i = 0; i < gameReady.usersList.length; i++) {
    comment.listMember.push({user: gameReady.usersList[i], score: 0});
  }
  io.sockets.emit('addNewUser', { gameReady });
  io.sockets.emit('commentAccost', { accost: comment.accost, users: gameReady.usersList });
  timer = setInterval(every30seconds, 30000);
}

function every30seconds() {
  io.sockets.emit('rating30sec', { ratingPlayers: comment.listMember });
}

io.on('connection', socket => {

  gameReady.race = races[Math.floor(Math.random() * 4)].text;

  socket.on('connectedNewUser', payload => {
    const { token } = payload;
    const userLogin = jwt.decode(token).login;
    gameReady.usersList.push(userLogin); 
    if (gameReady.usersList.length === 1) {
      setTimeout(playGame, 30000);
    }
  });

  socket.on('calcUserScore', payload => {
    const { score, token } = payload;
    const userLogin = jwt.decode(token).login;
    const scoreGame = score;
    for (let i = 0; i < comment.listMember.length; i++) {
      if (comment.listMember[i].user === userLogin) {
        comment.listMember[i].score = scoreGame;
      }
    }
    comment.listMember.sort((a,b) => (a.score > b.score) 
      ? -1 : ((b.score > a.score) 
      ? 1 : 0));
    io.sockets.emit('editUserScore',  { name: userLogin, score: scoreGame });
  });

  socket.on('ratingPlayers', () => {
    clearInterval(timer);
    socket.emit('rating30Leters', { ratingPlayers: comment.listMember });
  });

  socket.on('gameOver', payload => {
    comment.finishedCount++;
    let whoFinished = 'Наразі';
    const { token } = payload;
    const userLogin = jwt.decode(token).login;
    comment.listFinished.push(userLogin);
    console.log(comment.finishedCount + '===' + comment.listMember.length);
    if (comment.finishedCount === comment.listMember.length) {
      whoFinished = 'Останім';
      socket.emit('gameOverForUser', { name: userLogin });
      io.sockets.emit('gameOverForOther', { name: userLogin, whoFinished: whoFinished, finishList: comment.listFinished });
    } else if (comment.finishedCount < comment.listMember.length) {
      socket.emit('gameOverForUser', { name: userLogin });
      io.sockets.emit('gameOverForOther', { name: userLogin, whoFinished: whoFinished });
    }
  });

});